#!/bin/bash

DOCKER_IMAGE_TAG=${1:-latest}
DOCKER_IMAGE_REGISTRY=${2:-$(basename "$(pwd)")}

docker run --rm \
    --user $(id -u):$(id -g) \
    --mount type=bind,source="$(pwd)",destination=/build \
    --workdir /build \
    gradle \
    ./gradlew test
