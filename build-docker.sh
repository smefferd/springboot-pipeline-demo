#!/bin/bash

DOCKER_IMAGE_TAG=${1:-latest}
DOCKER_IMAGE_REGISTRY=${2:-$(basename "$(pwd)")}

mkdir -p build/doc
cp -r doc/assets build/doc/

docker run --rm \
    --user $(id -u):$(id -g) \
    --mount type=bind,source="$(pwd)",destination=/documents \
    asciidoctor/docker-asciidoctor \
    asciidoctor -v -o build/doc/index.html doc/doc.adoc

docker run --rm \
    --user $(id -u):$(id -g) \
    --mount type=bind,source="$(pwd)",destination=/build \
    --workdir /build \
    gradle \
    ./gradlew build

docker build --force-rm -t $DOCKER_IMAGE_REGISTRY:$DOCKER_IMAGE_TAG $(pwd)
