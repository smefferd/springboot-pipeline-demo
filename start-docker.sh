DOCKER_IMAGE_TAG=${1:-latest}
DOCKER_IMAGE_REGISTRY=${2:-$(basename "$(pwd)")}

docker run --rm -d -p 8080:8080 \
    --name $(basename $DOCKER_IMAGE_REGISTRY) \
    $DOCKER_IMAGE_REGISTRY:$DOCKER_IMAGE_TAG 

echo
echo Application demo url is http://localhost:8080/greeting?name=SpringBoot
