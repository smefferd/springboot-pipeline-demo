FROM registry.gitlab.com/digimountain/docker/java:openjdk8

EXPOSE 8080

COPY build/libs/ /app/
WORKDIR /app
CMD java -Dserver.port=$PORT -jar /app/*.jar

